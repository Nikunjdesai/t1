
import UIKit

extension UIViewController{
    func addContraints(button: UIButton, t: CGFloat, b: CGFloat, l: CGFloat, r: CGFloat) -> Array<Any>{
        button.translatesAutoresizingMaskIntoConstraints = false
    
        let topCon = button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: t)
        let BottomCon = button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: b)
        let leftCon  = button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: l)
        let rightCon = button.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: r)
    
        let array = [topCon,BottomCon,leftCon,rightCon]
        return array
    }
}
class ViewController: UIViewController
{
    
    let button1 = UIButton(frame: CGRect(x: 720 , y:440, width:327, height : 80))
    let button2 = UIButton(frame: CGRect(x: 720 , y:540, width:327, height : 80))
    let button3 = UIButton(frame: CGRect(x: 720 , y:640, width:327, height : 80))
    let musicButton = UIButton(frame: CGRect(x: 420 , y:690, width:50, height : 50))
    let soundButton = UIButton(frame: CGRect(x: 620 , y:690, width:50, height : 50))
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.darkGray
        
        var b1Cons:[NSLayoutConstraint] = []
        var b2Cons:[NSLayoutConstraint] = []
        var b3Cons:[NSLayoutConstraint] = []
        
        // button 1
        
        button1.setTitle("1", for: .normal)
        button1.backgroundColor = UIColor.white
        self.view.addSubview(button1)
        button1.setImage(UIImage(named:"btn_read.png"), for: .normal)
        button1.addTarget(self, action: #selector(buttonAction1), for: .touchUpInside)
        
        // button 2
        
        button2.setTitle("2", for: .normal)
        button2.backgroundColor = UIColor.white
        self.view.addSubview(button2)
        // button1.setImage(UIImage(named:""), for: .normal)
        button2.addTarget(self, action: #selector(buttonAction2), for: .touchUpInside)
        
        
        // button 3
        
        button3.setTitle("3", for: .normal)
        button3.backgroundColor = UIColor.white
        self.view.addSubview(button3)
        // button1.setImage(UIImage(named:""), for: .normal)
        button3.addTarget(self, action: #selector(buttonAction3), for: .touchUpInside)
        
        // musicButton
        
        musicButton.setTitle("Next", for: .normal)
        musicButton.backgroundColor = UIColor.white
        self.view.addSubview(musicButton)
        // button1.setImage(UIImage(named:""), for: .normal)
        musicButton.addTarget(self, action: #selector(musicButtonAction), for: .touchUpInside)
        
        // constrains for button 1
//        button1.translatesAutoresizingMaskIntoConstraints = false
//        
//        let topCon = button1.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 500)
//        let BottomCon = button1.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -500)
//        let leftCon  = button1.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 400)
//        let rightCon = button1.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -100)
//        
//        // add all the constrains into array
//        
//        b1Cons = [topCon,BottomCon,leftCon,rightCon]
        NSLayoutConstraint.activate(self.addContraints(button: button1, t: 150 , b: -550, l: 400, r: -100) as! [NSLayoutConstraint])
        NSLayoutConstraint.activate(self.addContraints(button: button2, t: 400, b: -300, l: 400, r: -100) as! [NSLayoutConstraint])
        NSLayoutConstraint.activate(self.addContraints(button: button3, t: 600, b: -100, l: 400, r: -100) as! [NSLayoutConstraint])
        
        
        
        
    }
   
    
    func buttonAction1(sender: UIButton!)
    {
       // navigationController?.pushViewController(secondScreenController, animated: true)
        print("do something")
        
    }

    func buttonAction2(sender: UIButton!)
    {
        // navigationController?.pushViewController(secondScreenController, animated: true)
        print("do something 2")
        
    }
    
    func buttonAction3(sender: UIButton!)
    {
        // navigationController?.pushViewController(secondScreenController, animated: true)
        print("do something 3")
        
    }
    func musicButtonAction(sender: UIButton!)
    {
        // navigationController?.pushViewController(secondScreenController, animated: true)
        print("music")
        
    }
}

